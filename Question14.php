<!DOCTYPE html>
<html lang="en">
<head>
    <style>

html {
    height: 100%;
  }
  body {
    margin:0;
    padding:0;
    font-family: sans-serif;
    background: linear-gradient(#141e30, #243b55);
  }
  
  .header{
    position: absolute;
    color: white;
    top: 25%;
    left: 50%;
    width: 800px;
    padding: 40px;
    transform: translate(-50%, -50%);
    background: rgba(0,0,0,.5);
    box-sizing: border-box;
    box-shadow: 0 15px 25px rgba(0,0,0,.6);
    border-radius: 10px;
    font-family: 'Courier New', Courier, monospace;
    letter-spacing: 1px;
    font-weight: bold;
    
  }
  .container {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 400px;
    padding: 40px;
    transform: translate(-50%, -50%);
    background: rgba(0,0,0,.5);
    box-sizing: border-box;
    box-shadow: 0 15px 25px rgba(0,0,0,.6);
    border-radius: 10px;
    font-family: 'Courier New', Courier, monospace;
  }
  
 
  .container .user-box {
    position: relative;
    color:white;
    font-weight:bolder;
    font-size: 20px;
    padding: 20px 20px;
  }
  
  .container .user-box input {
    width: 100%;
    padding: 10px 0;
    font-size: 16px;
    color: #fff;
    margin-bottom: 30px;
    border: none;
    border-bottom: 1px solid #fff;
    outline:none;
    background: transparent;
  }
  .container .user-box label {
    position: absolute;
    top:0;
    left: 0;
    padding: 10px 0;
    font-size: 16px;
    color: #fff;
    pointer-events: none;
    transition: .5s;
  }
  
  .container .user-box input:focus ~ label,
  .container .user-box input:valid ~ label {
    top: -20px;
    left: 0;
    color: #03e9f4;
    font-size: 12px;
  }
  .container form a {
        position: relative;
        display: inline-block;
        padding: 10px 90px;
        color: black;
        font-size: 16px;
        text-decoration: none;
        text-transform: uppercase;
        overflow: hidden;
        transition: .5s;
        margin-top: 40px;
        letter-spacing: 10px
      }
      
      .container a:hover {
        background:#141e30;
        color: #fff;
        border-radius: 5px;
        box-shadow: 0 0 5px rgb(63, 63, 110),
                    0 0 25px #141e30,
                    0 0 50pxn rgb(63, 63, 110),
                    0 0 100px #141e30;
      }
      
  .container a span {
    position: absolute;
    display: block;
  }
  
  .container a span:nth-child(1) {
    top: 0;
    left: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(90deg, transparent, #141e30);
    animation: btn-anim1 30s linear infinite;
  }
  
  @keyframes btn-anim1 {
    0% {
      right: -100%;
    }
    50%,100% {
      left: 100%;
    }
  }
  
  .container a span:nth-child(2) {
    top: -100%;
    right: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(180deg, transparent, #141e30);
    animation: btn-anim2 30s linear infinite;
    animation-delay: .25s
  }
  
  @keyframes btn-anim2 {
    0% {
      top: -100%;
    }
    50%,100% {
      top: 100%;
    }
  }
  
  .container a span:nth-child(3) {
    bottom: 0;
    right: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(270deg, transparent, #141e30);
    animation: btn-anim3 30s linear infinite;
    animation-delay: .5s
  }
  
  @keyframes btn-anim3 {
    0% {
      right: -100%;
    }
    50%,100% {
      right: 100%;
    }
  }
  
  .container a span:nth-child(4) {
    bottom: -100%;
    left: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(360deg, transparent, #141e30);
    animation: btn-anim4 30s linear infinite;
    animation-delay: .75s
  }
  
  @keyframes btn-anim4 {
    0% {
      bottom: -100%;
    }
    50%,100% {
      bottom: 100%;
    }
  }
  .submit{
  border-style: hidden;
  background-color: transparent;
  font-size: large;
  font-style:unset;
  color: white;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 10px;
  margin-bottom: 10px;
  letter-spacing:4px;
  
}
.submit:hover{
  color: #fff;
}
  
 

    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body> <div class="header">
    Question: 14. <br>	WAP to read the value of an integer m and display the value of n is 1 when m is larger than 0, 0 when m is 0 and -1 when m is less than 0.
</div>
<div class="container">
<form action="Question14.html" method="POST">
    
         
       
          <div class="user-box">
            <?php
          if($_SERVER['REQUEST_METHOD']=='POST'){
    $m = $_POST['m'];

    echo "Input: m = ".$m."<br><br>";
    if($m > 0){
        $n = 1;
        echo "OUTPUT: n = ".$n."<br>";
    }elseif($m < 0){
        $n = -1;
        echo "OUTPUT: n = ".$n."<br>";
    }elseif($m == 0){
        $n = 0;
        echo "OUTPUT: n = ".$n."<br>";
    }
   

    
}
?>
        </div>
          <a>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <input type="submit" name="Submit" value="GO BACK" class="submit">
            
          </a>
        </form>
      </div>
</body>
</html>
<?php
